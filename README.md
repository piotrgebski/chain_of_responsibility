#Łańcuch odpowiedzialności (wzorzec projektowy)

***Łańcuch odpowiedzialnośc*** - czynnościowy wzorzec projektowy, w którym żądanie może być przetwarzane przez różne obiekty, w zależności od jego typu.

*Cel zastosowania łańcuchu odpowiedzialności* 

- Usunięcie powiązania pomiędzy nadawcą i odbiorcą żądania
- Umożliwienie wielu obiektom obsługi żądania
- Komunikat przekazywany jest pomiędzy obiektami należącymi do pewnego zbioru zgodnie z precyzyjnie wyznaczoną trasą i kolejnością.
- Odpowiedzialność za przetworzenie komunikatu spada na obiekt, który jest do tego zadania najlepiej przygotowany.

###Przykład zastosowania 

Rozpatrzmy drzewo dokumentu (DOM). Każdy element może mieć elementy - dzieci, oraz ma swój element - rodzica w którym się zawiera (poza elementem głównym - rootem).

Użytkownik klika na któryś z elementów w drzewie. Jest to zarówno kliknięcie na ten element, jak i na elementy w których się znajduje ten element (rodziców - ponieważ znajdują się pod klikniętym elementem i kliknięcie może również dotyczyć ich). Elementy mogą mieć handler dla zdarzenia click - lub nie. Jeżeli obiekt posiada handler, obsługuje kliknięcie, i propagacja się kończy. Jeżeli handler uzna, że nie jest w stanie obsłużyć zdarzenia i zostanie wywołana odpowiednia metoda powodująca propagację wyżej, lub jeżeli obiekt nie posiada handlera, zdarzenie jest propagowane do rodzica klikniętego elementu. Cykl się powtarza dla rodzica - znowu, jeżeli potrafi on obsłużyć zdarzenie to propagacja się kończy, jeżeli nie, zdarzenie jest propagowane wyżej i wyżej, tak długo aż któryś z obiektów je obsłuży, lub propagacja osiągnie root element.

##Budowa

Wzorzec "Łańcuch odpowiedzialności" zakłada utworzenie oddzielnej klasy dla każdej procedury obsługi żądania dziedziczącej po pewnej klasie bazowej AbstrakcyjnaObsluga. Obiekt każdej z procedur może posiadać wskazanie na następnik, tworząc w ten sposób łańcuch procedur przetwarzania. Aby przetworzyć żądanie, wykonujemy metodę operacja() na pierwszym elemencie łańcucha. Jeśli nie potrafi on przetworzyć żądania, powinien przekazać je swojemu następnikowi:

## Konsekwencje stosowania

*Zalety:*

Zaletą tego wzorca jest znaczne ograniczenie powiązań pomiędzy klientem i każdym z obiektów Handler. Klient, przekazując żądanie, nie wie, który z obiektów Handler będzie je w rzeczywistości obsługiwał. Poszczególne ogniwa łańcucha są zorganizowane w postaci prostej kolejki jednokierunkowej, a ich wiedza o sobie nawzajem ogranicza się do abstrakcyjnego typu ogniwa. Nie znają swoich zadań ani klas, jakie implementują. Taka struktura pozwala elastycznie przydzielać odpowiedzialność do poszczególnych ogniw: każdy z nich zajmuje się obsługą żądań jednego typu, a rozszerzenie łańcucha o kolejne elementy nie wpływa na sposób przetwarzania przez niego żądań. To z kolei przyczynia się do łatwiejszego testowania każdego ogniwa łańcucha z osobna: wystarczy zweryfikować, czy poprawnie obsługuje on żądania jednego typu.

*Wady*


Wadą takiej konstrukcji łańcucha jest brak gwarancji obsługi żądania: kolejne ogniwa mogą zrezygnować z zajęcia się nim. Co więcej, informacja o tym fakcie nie jest przekazywana klientowi. W tym celu stosuje się rozmaite rozwiązania pośrednie: umieszczając informację o obsłudze wewnątrz żądania (wówczas brak takiej informacji oznacza jego nieobsłużenie) lub zmieniając nieco strukturę przetwarzania. Ponadto, błąd w implementacji filtra może skutkować nieprzekazaniem sterowania do następnika i przerwaniem łańcucha. Aby zminimalizować to ryzyko, w niektórych implementacjach klasa bazowa Filter posiada zaimplementowany na stałe mechanizm przekazywania sterowania do następnika, a programiście udostępniona jest tylko metoda dokonująca faktycznej obsługi żądania. Występują dwie odmiany wzorca: jedna, w której w przypadku znalezienia procedury obsługi żądanie nie jest dalej przesyłane, i druga, gdzie przetwarzanie jest kontynuowane niezależnie od tego, czy znaleziono właściwą procedurę obsługi. Drugi wariant, gdzie zdarzają się sytuacje wywołania wielu procedur obsługi, jest szczególnie trudny do diagnozowania. W przypadku wzorca Łańcuch odpowiedzialności potwierdza się reguła, że rozbudowane możliwości programów komputerowych osiąga się kosztem złożoności i wydajności.


###Źródło 
 - [Wikipedia](https://pl.wikipedia.org/wiki/%C5%81a%C5%84cuch_zobowi%C4%85za%C5%84_(wzorzec_projektowy))
 - [AGH.EDU](http://zasoby.open.agh.edu.pl/~09sbfraczek/lancuch-zobowiazan%2C1%2C44.html)